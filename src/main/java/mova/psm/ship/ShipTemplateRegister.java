package mova.psm.ship;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.POJONode;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import mova.game.core.FatalError;
import mova.game.geom.Locator;
import mova.game.geom.Segment;
import mova.game.graphics.geom.GeometryUtils;
import mova.game.graphics.geom.PathSegment;
import mova.game.graphics.geom.PathSegments;
import mova.game.graphics.geom.ShapeUtils;
import mova.lang.MathUtils;
import mova.util.ArrayUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
public class ShipTemplateRegister {

    private static final ObjectMapper mapper = new ObjectMapper();

    static {
        SimpleModule deserializersModule = new SimpleModule();
        deserializersModule.addDeserializer(Locator.class, new DoublesToLocator());
        deserializersModule.addDeserializer(Segment.class, new DoublesToSegment());
        mapper.registerModule(deserializersModule);

        SimpleModule serializersModule = new SimpleModule();
        serializersModule.addSerializer(Locator.class, new LocatorToJson());
        serializersModule.addSerializer(Segment.class, new SegmentToJson());
        serializersModule.addSerializer(PathSegment.class, new PathSegmentToJson());
        mapper.registerModule(serializersModule);
    }

    private static final JavaType BOARD_TYPE = mapper.getTypeFactory().constructType(new TypeReference<Map<ShipSide, Locator[]>>() {
    });
    private static final JavaType SAILS_TYPE = mapper.getTypeFactory().constructType(new TypeReference<Segment[]>() {
    });

    private final EnumMap<ShipType, ShipTemplate> shipTemplates = new EnumMap<>(ShipType.class);

    @PostConstruct
    public void load() {
        // TODO y'a peut être moyen de tout mettre dans un seul fichier json en fait...
        load(type -> {
            String filename = type.name().toLowerCase().replace('_', '-') + ".json";
            return getClass().getResource(filename);
        });
    }

    public void load(Function<ShipType, URL> urlProvider) {
        for (ShipType type : ShipType.values()) {
            URL url = urlProvider.apply(type);

            if (url == null) throw new FatalError(type + " file not found");

            try {
                JsonNode node = mapper.readTree(url);

                PathSegment[] segments = mapper.treeToValue(node.get("hull"), PathSegment[].class);
                Path2D hull = ShapeUtils.construcPath(segments);

                Locator[] masts = mapper.treeToValue(node.get("masts"), Locator[].class);
                Segment[] sails = mapper.treeToValue(node.get("sails"), SAILS_TYPE);

                Map<ShipSide, Locator[]> boards = mapper.readValue(mapper.treeAsTokens(node.get("boards")), BOARD_TYPE);

                shipTemplates.put(type, new ShipTemplate(hull, ArrayUtils.invert(masts), sails, boards));
            } catch (IOException ioe) {
                throw new FatalError(type + " file can't be loaded", ioe);
            }
        }
    }

    public ShipTemplate getShipTemplate(ShipType type) {
        return shipTemplates.get(type);
    }

    private static class DoublesToLocator extends StdDeserializer<Locator> {
        protected DoublesToLocator() {
            super(Locator.class);
        }

        @Override
        public Locator deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            Double[] doubles = jsonParser.readValueAs(Double[].class);

            return new Locator(doubles[0], doubles[1]);
        }
    }

    private static class DoublesToSegment extends StdDeserializer<Segment> {
        protected DoublesToSegment() {
            super(Segment.class);
        }

        @Override
        public Segment deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            Double[][] doubles = jsonParser.readValueAs(Double[][].class);

            return new Segment(doubles[0][0], doubles[0][1], doubles[1][0], doubles[1][1]);
        }
    }

    private static class LocatorToJson extends StdSerializer<Locator> {
        public LocatorToJson() {
            super(Locator.class);
        }

        @Override
        public void serialize(Locator value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeArray(new double[]{(float) value.getX(), (float) value.getY()}, 0, 2);
        }
    }

    private static class SegmentToJson extends StdSerializer<Segment> {
        public SegmentToJson() {
            super(Segment.class);
        }

        @Override
        public void serialize(Segment value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeStartArray();
            gen.writeObject(value.getP1());
            gen.writeObject(value.getP2());
            gen.writeEndArray();
        }
    }

    private static class PathSegmentToJson extends StdSerializer<PathSegment> {

        public PathSegmentToJson() {
            super(PathSegment.class);
        }

        private int coordsLength(PathSegment pathSegment) {
            switch (pathSegment.getType()) {
                case 'M':
                case 'L':
                    return 2;
                case 'Q':
                    return 4;
                case 'C':
                    return 6;
                case 'X':
                    return 0;
                default:
                    throw new IllegalStateException();
            }
        }

        @Override
        public void serialize(PathSegment value, JsonGenerator gen, SerializerProvider provider) throws IOException {
            gen.writeStartObject();
            gen.writeStringField("type", String.valueOf(value.getType()));

            gen.writeFieldName("coordinates");
            int length = coordsLength(value);
            if (length == 0) gen.writeNull();
            else {
                gen.writeArray(MathUtils.toDoubles(value.getCoordinates()), 0, length);
            }
            gen.writeEndObject();
        }
    }

    private static float getTranslation(ShipTemplate shipTemplate) {
        Path2D hull = shipTemplate.getHull();
        return -(float) GeometryUtils.getBounds2D(hull).getWidth() / 2;
    }

    private static void translateShip(ShipTemplate shipTemplate, float dx) {
        AffineTransform translation = AffineTransform.getTranslateInstance(dx, 0);

        Path2D hull = shipTemplate.getHull();
        hull.transform(translation);

        for (Locator mast : shipTemplate.getMasts()) {
            translation.transform(mast, mast);
        }

        for (Segment sail : shipTemplate.getSails()) {
            sail.setLine(translation.transform(sail.getP1(), null), translation.transform(sail.getP2(), null));
        }

        for (Map.Entry<ShipSide, Board> shipSideBoardEntry : shipTemplate.getBoards().entrySet()) {
            Board board = shipSideBoardEntry.getValue();
            board.setLine(translation.transform(board.getP1(), null), translation.transform(board.getP2(), null));
        }
    }

    private static String map(String prefix, JsonNode node) {
        try {
            return prefix + mapper.writeValueAsString(node);
        } catch (JsonProcessingException e) {
            throw new FatalError(e.getMessage());
        }
    }

    private static void writeResult(ShipTemplate shipTemplate) {
        ObjectNode shipNode = new ObjectNode(mapper.getNodeFactory());

        console("{\n  \"hull\": [");
        Path2D hull = shipTemplate.getHull();
        String hullString = PathSegments.stream(hull.getPathIterator(null)).map(pathSegment -> map("    ", new POJONode(pathSegment))).collect(Collectors.joining(",\n"));
        console(hullString);
        console("  ],");
        console("  \"masts\": [");
        String mastsString = Arrays.stream(ArrayUtils.invert(shipTemplate.getMasts())).map(mast -> map("    ", new POJONode(mast))).collect(Collectors.joining(",\n"));
        console(mastsString);
        console("  ],");
        console("  \"sails\": [");
        String sailsString = Arrays.stream(shipTemplate.getSails()).map(sail -> map("    ", new POJONode(sail))).collect(Collectors.joining(",\n"));
        console(sailsString);
        console("  ],");
        console("  \"boards\": {");
        String boardsString = shipTemplate.getBoards().entrySet().stream().map(board -> map("    \"" + board.getKey() + "\": ", new POJONode(board.getValue()))).collect(Collectors.joining(",\n"));
        console(boardsString);
        console("  }");
        console("}");

        console("\n");
    }

    private static void console(String s) {
        System.out.println(s);
    }

    public static void main(String[] args) {
        ShipTemplateRegister register = new ShipTemplateRegister();
        register.load(type -> {
            String filename = type.name().toLowerCase().replace('_', '-') + ".json";
            try {
                return Paths.get("C:\\Developpement\\Projets\\Game\\PSM\\psm-graphics\\src\\main\\resources\\mova\\psm\\ship", filename).toUri().toURL();
            } catch (MalformedURLException e) {
                throw new FatalError(e.getMessage());
            }
        });

        for (ShipType type : ShipType.values()) {
            ShipTemplate shipTemplate = register.getShipTemplate(type);

            float dx = getTranslation(shipTemplate);
            console("type = " + type + "\t\t=>\t\tdx = " + dx);
            translateShip(shipTemplate, dx);
            writeResult(shipTemplate);
        }
    }
}
