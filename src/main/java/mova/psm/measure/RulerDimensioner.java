package mova.psm.measure;

import mova.game.core.ecs.AbstractGameComponent;
import mova.game.geom.Locator;
import mova.game.geom.Segment;
import mova.game.graphics.RenderableGroup;
import mova.game.graphics.ShapeRenderer;
import mova.game.graphics.Transform;
import mova.game.input.GameInputsListener;
import mova.game.view.Projection;
import mova.psm.ship.Range;

public class RulerDimensioner extends AbstractGameComponent {

    private static final Locator O = new Locator();

    private final GameInputsListener gameInputsListener;
    private AngleConstraints maneuverRange = null;
    private Locator to = O;
    private Range range = Range.S;

    private boolean autoUpdate = false;

    public RulerDimensioner(GameInputsListener gameInputsListener) {
        this.gameInputsListener = gameInputsListener;
    }

    public void autoUpdate(boolean enabled) {
        autoUpdate = enabled;
    }

    public void setManeuverRange(AngleConstraints maneuverRange) {
        this.maneuverRange = maneuverRange;
    }

    public void setTo(Locator to) {
        this.to = to;
    }

    public void setRange(Range range) {
        this.range = range;

        RenderableGroup shapes = getGameObject().getComponent(RenderableGroup.class);
        shapes.getShapeRenderer("L").setVisible(range == Range.L);
    }

    @Override
    public void update(long dt) {
        if (autoUpdate) {
            Projection screenProjection = getGameObject().getScene().getCamera().getScreenProjection();
            Locator worldLocation = screenProjection.inverseTransform(gameInputsListener.getMouseLocation());
            setTo(worldLocation);
        }

        Transform transform = getGameObject().getComponent(Transform.class);
        Locator from = transform.apply(O);

        Locator constrainedTo = maneuverRange != null ? maneuverRange.apply(from, to) : to;

        RenderableGroup shapes = getGameObject().getComponent(RenderableGroup.class);
        if (range == Range.L) {
            Locator currentCourse = Ruler.capToRange(from, constrainedTo, Range.L);
            ((Segment) shapes.getShapeRenderer("L", ShapeRenderer.class).getShape()).setLine(O, transform.inverseApply(currentCourse));
        }

        Locator currentCourse = Ruler.capToRange(from, constrainedTo, Range.S);
        ((Segment) shapes.getShapeRenderer("S", ShapeRenderer.class).getShape()).setLine(O, transform.inverseApply(currentCourse));
    }
}
