package mova.psm.ship;

import mova.game.core.Copyable;
import mova.game.geom.Locator;
import mova.game.geom.Segment;
import mova.trigo.TrigoUtils;

public class Board extends Segment implements Copyable<Board> {

    private final ShipSide side;

    public Board(float x1, float y1, float x2, float y2, ShipSide side) {
        super(x1, y1, x2, y2);
        this.side = side;
    }

    public Board(Locator p1, Locator p2, ShipSide side) {
        super(p1, p2);
        this.side = side;
    }

    private Board(Board board) {
        super(board.getPoupe(), board.getProue());
        side = board.side;
    }

    public ShipSide getSide() {
        return side;
    }

    public Locator getProue() {
        return getProue(this);
    }

    public Locator getPoupe() {
        return getPoupe(this);
    }

    public double direction() {
        return direction(this);
    }

    @Override
    public Board copy() {
        return new Board(this);
    }

    public static Locator getProue(Segment board) {
        return board.getP2();
    }

    public static Locator getPoupe(Segment board) {
        return board.getP1();
    }

    public static double direction(Segment board) {
        return TrigoUtils.atan2(getPoupe(board), getProue(board));
    }
}
