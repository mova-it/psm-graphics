package mova.psm.ship;

import mova.game.geom.Locator;
import mova.game.geom.Segment;

import java.awt.geom.Path2D;
import java.util.EnumMap;
import java.util.Map;

public class ShipTemplate {

    private final Path2D hull;
    private final Locator[] masts;
    private final Segment[] sails;
    private final EnumMap<ShipSide, Board> boards = new EnumMap<>(ShipSide.class);

    ShipTemplate(Path2D hull, Locator[] masts, Segment[] sails, Map<ShipSide, Locator[]> boards) {
        this.hull = hull;
        this.masts = masts;
        this.sails = sails;

        for (ShipSide shipSide : ShipSide.values()) {
            Locator[] points = boards.get(shipSide);
            this.boards.put(shipSide, new Board(points[0], points[1], shipSide));
        }
    }

    public Path2D getHull() {
        return hull;
    }

    public Locator[] getMasts() {
        return masts;
    }

    public Locator getMast(int i) {
        return masts[i];
    }

    public Map<ShipSide, Board> getBoards() {
        return boards;
    }

    public Segment[] getSails() {
        return sails;
    }
}
