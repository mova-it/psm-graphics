package mova.psm.measure;

import mova.game.geom.Locator;
import mova.game.graphics.geom.GeometryUtils;
import mova.trigo.TrigoUtils;

import java.util.ArrayList;
import java.util.List;

public class AngleConstraints {
    private final double minPhi;
    private final double maxPhi;

    public AngleConstraints(double minPhi, double maxPhi) {
        this.minPhi = minPhi;
        this.maxPhi = maxPhi;
    }

    public Locator apply(Locator from, Locator mouseLocation) {
        double phi = TrigoUtils.atan2(from, mouseLocation);
        phi = TrigoUtils.normalize(phi);

        boolean outOfBounds = TrigoUtils.isOutOfBounds(phi, minPhi, maxPhi);

        if (!outOfBounds) return mouseLocation;

        return getNearestProjection(mouseLocation, from, minPhi, maxPhi);
    }

    private static Locator getNearestProjection(Locator mouseLocation, Locator from, double minPhi, double maxPhi) {
        List<Locator> points = new ArrayList<>();
        getEligibleProjections(points, mouseLocation, from, minPhi);
        getEligibleProjections(points, mouseLocation, from, maxPhi);

        if (points.size() == 1) return points.get(0);

        return GeometryUtils.closest(points, mouseLocation);
    }

    private static void getEligibleProjections(List<Locator> points, Locator mouseLocation, Locator from, double phi) {
        double tan = Math.tan(phi);

        double dx = mouseLocation.getX() - from.getX();
        Locator xProjection = new Locator(mouseLocation.getX(), dx*tan + from.getY());
        if (xProjection.equals(from) || hasSameQuadrant(xProjection, from, phi)) points.add(xProjection);

        double dy = mouseLocation.getY() - from.getY();
        Locator yProjection = new Locator(dy/tan + from.getX(), mouseLocation.getY());
        if (yProjection.equals(from) || hasSameQuadrant(yProjection, from, phi)) points.add(yProjection);
    }

    private static boolean hasSameQuadrant(Locator p, Locator from, double phi) {
        int minPhiQuadrant = TrigoUtils.quadrant(phi); // TODO y'a peut être moyen de s'en srotir avec Line2D#relativeCCW
        int pQuadrant = TrigoUtils.quadrantFrom(p, from);

        if (pQuadrant == minPhiQuadrant) {
            if (minPhiQuadrant == 0) {
                if (phi == TrigoUtils.PI_SUR_DEUX && p.getY() > from.getY()) return true;
                else if (phi == -TrigoUtils.PI_SUR_DEUX && p.getY() < from.getY()) return true;
                else if (phi == 0.0 && p.getX() > from.getX()) return true;
                else return p.getX() < from.getX();
            } else {
                return true;
            }
        }

        return false;
    }
}
