package mova.psm.measure;

import mova.game.core.FatalError;
import mova.game.geom.Locator;
import mova.psm.ship.Range;

import java.util.EnumMap;
import java.util.Properties;

public class Ruler {

    // TODO ici on confond ce qui relèvre d'un module graphique et ce qui est de la cartographie (qui implique donc des mesures graphiques)
    //  La séparation va pas être facile à faire, mais d'emblée je dirais que la mesure à plus sa place dans core ou model que dans graphic

    private static final EnumMap<Range, Integer> lengthByRange = new EnumMap<>(Range.class);
    static {
        Properties properties = new Properties();
        try {
            properties.load(Ruler.class.getResourceAsStream("lengths.properties"));
        } catch (Exception e) {
            throw new FatalError("Impossible de lire le fichier \"lengths.properties\"", e);
        }

        lengthByRange.put(Range.S, Integer.valueOf(properties.getProperty("S")));
        lengthByRange.put(Range.L, Integer.valueOf(properties.getProperty("L")));
    }

    private Ruler() {}

    public static int getRangeLength(Range range) {
        return lengthByRange.get(range);
    }

    public static Locator capToRange(Locator from, Locator to, Range range) {
        return capToRange(from, to, getRangeLength(range));
    }

    public static Locator capToRange(Locator from, Locator to, int length) {
        double dy = to.getY() - from.getY();
        double dx = to.getX() - from.getX();
        double hypothenus = Math.sqrt(dx * dx + dy * dy);

        if (hypothenus > length) {
            double ratio = length / hypothenus;
            dx *= ratio;
            dy *= ratio;
        }

        return new Locator(dx + from.getX(), dy + from.getY());
    }


}
