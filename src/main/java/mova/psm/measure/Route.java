package mova.psm.measure;

import mova.game.core.Copyable;
import mova.game.geom.Locator;
import mova.trigo.TrigoUtils;
import mova.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

public class Route implements Copyable<Route> {

    private final ArrayList<Locator> courses = new ArrayList<>();

    public Route(Locator origin) {
        courses.add(origin);
    }

    private Route(Route route) {
        courses.addAll(route.courses);
    }

    public void resetTo(Locator origin) {
        courses.clear();
        courses.add(origin);
    }

    public void addCourse(Locator course) {
        courses.add(course);
    }

    public boolean isEmpty() {
        // Si la route n'a qu'un cap c'est celui de l'origin
        return courses.size() == 1;
    }

    public Locator getCurrentCourse() {
        return CollectionUtils.last(courses);
    }

    public double getLastDirection() {
        Locator from = courses.get(courses.size() - 2);
        Locator to = courses.get(courses.size() - 1);
        return TrigoUtils.atan2(from, to);
    }

    public boolean removeLastCourse() {
        if (courses.size() > 1) {
            courses.remove(courses.size() - 1);
            return true;
        }

        return false;
    }

    public Locator getOrigin() {
        return courses.get(0);
    }

    public List<Locator> getCourses() {
        // On renvoie tous les caps sauf le premier qui correspond à l'origin de la route
        return new ArrayList<>(courses.subList(1, courses.size()));
    }

    @Override
    public Route copy() {
        return new Route(this);
    }
}
