package mova.psm.map;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import mova.game.core.FatalError;
import mova.game.graphics.geom.PathSegment;
import mova.game.graphics.geom.ShapeUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.awt.*;
import java.awt.geom.Path2D;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

@Component
public class LandRegister {

    private final Map<String, Shape> landShapes = new HashMap<>();

    @PostConstruct
    private void load() {
        URL url = getClass().getResource("lands.json");
        load(url);
    }
    private void load(URL url) {
        if (url == null) throw new FatalError("lands.json not found");

        ObjectMapper mapper = new ObjectMapper();

        try {
            JsonNode nodes = mapper.readTree(url);

            for (JsonNode landNode : nodes) {
                String name = landNode.get("name").asText();
                PathSegment[] segments = mapper.treeToValue(landNode.get("shape"), PathSegment[].class);
                Path2D hull = ShapeUtils.construcPath(segments);

                landShapes.put(name, hull);
            }
        } catch (IOException ioe) {
            throw new FatalError("lands.json can't be loaded", ioe);
        }
    }

    public Shape getLandShape(String name) {
        return landShapes.get(name);
    }

    public static void main(String[] args) {
        // Note: Pour l'enregistrement des îles après dessins sur SVGPathEditor
        String command = "M 0 -32 C 113 -94 269 -94 350 0 C 269 94 113 94 0 32 L 0 -32 M 34 0 L 160 0 L 380 0 M 280 -32 L 280 32 Z";
        String json = ShapeUtils.toJson(command);

        System.out.println(json);
    }
}
